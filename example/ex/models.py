from django.db import models

# Create your models here.

class Test(models.Model):
    class Meta:
        verbose_name = 'Test model'
