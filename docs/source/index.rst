Welcome to Django Translations Management documentation!
=================================================

Simple replacement for rozetta module for just to be able to download/upload translation files.


Installation
------------

Installing from gitlab. ::

    pip install git+https://gitlab.com/preusx/django-translations-management.git@1.0.0


Contents
--------

.. toctree::
   :maxdepth: 2

   utils
   changelog
