from .utils import re_path
from . import views

app_name = 'dtm'

urlpatterns = [
    re_path('^$', views.translations_list, name='list'),
    re_path('^download/$', views.download, name='download'),
    re_path('^upload/$', views.upload, name='upload')
]
