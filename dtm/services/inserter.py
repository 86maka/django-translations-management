from polib import pofile

from dtm.signals import post_insert


def save_file(path, file):
    with open(path, 'wb') as out:
        out.write(file.read())


def compile_pofile(path):
    po = pofile(path)
    po.save_as_mofile(path[:-3] + '.mo')


def insert(path, file):
    save_file(path, file)
    compile_pofile(path)

    post_insert.send(sender=None, path=path)
