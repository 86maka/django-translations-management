import os
from contextlib import suppress

from django.http import Http404
from django import forms
from django.shortcuts import redirect

from django_sendfile import sendfile

from dtm.utils import only_superuser, reverse
from .utils import FilePathForm, handle_form


@only_superuser
def download(request):
    form = FilePathForm(request.GET)

    with suppress(forms.ValidationError):
        with handle_form(request, form):
            path = form.cleaned_data['path']
            name = '-'.join(path.split('/')[-5:])

            return sendfile(
                request, path, attachment=True, attachment_filename=name
            )

    return redirect(reverse('dtm:list'))
