import os
from itertools import chain
from contextlib import contextmanager

from django.contrib import messages
from django.conf import settings
from django.utils.translation import gettext_lazy as _
from django import forms

from dtm.services import locator


__all__ = (
    'get_available_languages',
    'get_available_files',
    'is_available',
    'message_form_errors',
    'handle_form',
    'FilePathForm'
)


def get_available_languages():
    return {
        code: _(name) for code, name in settings.LANGUAGES
    }


def get_available_files():
    located = locator.locate(
        languages=get_available_languages(),
        is_django=True,
        is_project=True,
        is_third_party=True
    )

    return set(chain(*(
        files for language, files in located.items()
    )))


def is_available(file):
    return file in get_available_files()


def message_form_errors(request, form):
    for key, errors in form.errors.items():
        for error in errors:
            messages.add_message(request, messages.ERROR, error)


@contextmanager
def handle_form(request, form, success_message=None):
    valid = form.is_valid()

    if not valid:
        message_form_errors(request, form)
        raise forms.ValidationError(None)

    try:
        yield form

        if (success_message is not None):
            messages.add_message(request, messages.SUCCESS, success_message)

    except Exception as e:
        messages.add_message(
            request, messages.ERROR, 'Internal error: {}'.format(e)
        )


class FilePathForm(forms.Form):
    path = forms.CharField(required=True)

    def clean_path(self):
        value = self.cleaned_data.get('path', '')

        if not os.path.isfile(value) or not is_available(value):
            raise forms.ValidationError('Wrong file destination provided.')

        if not os.access(value, os.W_OK):
            raise forms.ValidationError('File is not writable.')

        return value