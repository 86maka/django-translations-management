"""
Utility functions and classes to use in library.
"""
from django.contrib.auth.decorators import user_passes_test


__all__ = [
    're_path',
    'include',
    'path_regex',
    'reverse'
]

try:
    from django.shortcuts import reverse
except ImportError as e:
    from django.core.urlresolvers import reverse

try:
    from django.urls import re_path, include
except ImportError as e:
    from django.conf.urls import url, include as old_include

    re_path = url

    def _is_list(x):
        return isinstance(x, (list, tuple))

    def include(lst):
        """
        Mock of the old Django's `include` to be able to namespace the
        views in a 2.0 way.
        """

        if _is_list(lst) and len(lst) > 0 and _is_list(lst[0]):
            return old_include(lst[0], namespace=lst[1])

        return old_include(lst)


def path_regex(path):
    """
    Compatibility regex getter from Django's UrlPattern.

    Args:
        path (UrlPattern): Django's UrlPattern object provided by
            `re_path`, or in old versions `url`.

    Returns:
        str: Regex from this pattern object.
    """

    return path.pattern.regex if hasattr(path, 'pattern') else path.regex


only_superuser = user_passes_test(lambda u: u.is_superuser)
