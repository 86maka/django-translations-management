from django.apps import AppConfig


class DtmAppConfig(AppConfig):
    name = "dtm"

    def ready(self):
        from django.contrib import admin

        admin.site.index_template = "dtm/admin_index.html"
